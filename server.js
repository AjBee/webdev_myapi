var express = require('express');
var cors = require('cors');
var app = express();
var mysql = require('mysql');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors({ origin: true }));
app.use(cors());

const dbConn = mysql.createConnection({
    host: 'se.mfu.ac.th',
    user: 'wd21lab',
    password: 'wd21lab@2021',
    database: 'wd21lab_db'
});

dbConn.connect((err) => {
    if (err) throw err;
    console.log('Mysql Connected...');
});

//http://localhost:3000/hello
//Tesging API Server
app.get('/hello', function (req, res, next) {
    return res.send({ success: true, message: 'hello' })
});

/*----CURL for Testing Get All ----
curl http://localhost:3000/api/products
----- */  
// Retrieve all products 
app.get('/api/products', function (req, res) {
    dbConn.query('SELECT * FROM products', function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Get All Products' });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }

    });
});

/*----CURL for Testing Get by ID ----
curl http://localhost:3000/api/products/1
----- */  
// Retreive product by id
app.get('/api/products/:id', function (req, res) {
    let id = req.params.id;
    dbConn.query('SELECT * FROM products WHERE id = ? ', [id], function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Get Product by ID ' + id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});


/*----CURL for Testing New Product----
curl  \
 -i -X POST \
 -d "id=16&name=test" \
  http://localhost:3000/api/products
----- */  
// Add a new Product 
app.post('/api/products', function (req, res) {

    let product = req.body;
    console.log(req.body);
    dbConn.query("INSERT INTO products SET ? ", product, function (error, results, fields) {
        try {
            console.log(product);
            return res.send({ error: false, data: results, message: 'New Product successfully.' });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }

    });
});

/*---- CURL for Testing Update ----
curl  \
 -i -X PUT \
 -d "name=test123&price=205" \
  http://localhost:3000/api/products/16
----- */
// Update Product 
app.put('/api/products/:id', function (req, res) {
    let id = req.params.id;
    let product = req.body;
    // console.log(req);
    dbConn.query("UPDATE products SET ? WHERE id=?", [product, id], function (error, results, fields) {
        try {
            console.log(product);
            return res.send({ error: false, data: results, message: 'Update Product '+id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }

    });
});

/*---- CURL for Testing Delete ----
curl  \
 -i -X DELETE \
http://localhost:3000/api/products/16
----- */
// Retreive product by id
app.delete('/api/products/:id', function (req, res) {
    let id = req.params.id;
    dbConn.query('DELETE FROM products WHERE id = ? ', [id], function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Delete Product by ID ' + id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});

app.listen(process.env.PORT || 3000, function () {
    console.log('Node app is running on port 3000');
});
module.exports = app;

